$(document).ready(function () {
  //     let products = [];

  //     $.getJSON("products.json", function (data) {
  //         $.each(data, function (index, product) {
  //             products.push(product);
  //         });
  //     })
  
  //         .done(function () {
  //             $("#target").keyup(function () {
  //                 let searchText = $('#target').val()

  //                 let filteredProducts = _.filter(products, function (product) { return product.name.toLowerCase().includes(searchText.toLowerCase()) || product.brand.toLowerCase().includes(searchText.toLowerCase()) });
  //                 $("#result").html('');
  //                 // Print the filtered products

  //                 $.each(filteredProducts, function (key, product) {
  //                     $('#result').append('<li class="list-group-item link-class"> ' + product.name + ' | <span class="text-muted">' + product.brand + '</span></li>');
  //                 })
  //             });
  //             $('#result').on('click', 'li', function () {
  //                 let click_text = $(this).text().split('|');
  //                 $('#target').val($.trim(click_text[0]));
  //                 $("#result").html('');
  //             });
  //         })
  // ===================================basket===================================
  let products = []
  let subtotal = 0

  $('.add-btn').click(function () {
    $('.success-message').show()
    let incrementedValue = +$('.numbers').text() + 1;
    $('.numbers').text(incrementedValue);

    let name = $(this).data('name')
    let details = $(this).data('details')
    let price = $(this).data('price')
    let img = $(this).data('img')
    subtotal += parseFloat(price)
    text = subtotal
    $('#subtotal').html(`Subtotal: ${text}`)
    let productObject = {
      brand: name,
      name: details,
      price: price,
      img: img
    }
    products.push(productObject)

    $('#products').html('')

    $.each(products, function (key, product) {
      $('#products').append(`<div id="product-${key}" class="products-popup d-flex justify-content-around align-items-center border-bottom "> <div> <img class="img-fluid img-basket" src="${product.img}" alt=""> </div> <div class="w-50 text-center"> <h6>${product.brand}</h6> <p>${product.name}</p> </div> <div> <p class="font-weight-bold">${product.price} BGN</p></div> <div><button type="button" class="border bg-white remove-product" data-price="${product.price}" data-id="${key}"><img src="./images/icons/close.svg" alt=""></button></div> </div>`)
    })
    $('.remove-product').click(function () {
      subtotal -= $(this).data('price')
      $('#subtotal').html(`Subtotal: ${subtotal}`)
      products.splice($(this).data('id'), 1)
      $('#product-' + $(this).data('id')).remove()
      let incrementedValue = +$('.numbers').text() - 1;
      $('.numbers').text(incrementedValue);
      if (incrementedValue <= 0) {
        $('.numbers').text('');
      }
    })
  })
  $('.close-success-message').click(function () {
    $('.success-message').hide()
  })
  $('#show-pop').click(function () {
    $('#popup-basket').show()
  })
  $('.close-popup').click(function () {
    $('#popup-basket').hide()
  })

})
